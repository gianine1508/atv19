import React from "react";
import Fruta from "./components/fruta";
import FrutaCor from "./components/frutaCor";
import FrutaRed from "./components/frutaRedu";

class App extends React.Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ];

  render() {
    return (
      <div className="App">
        <div id="all fruit names">
          <Fruta fruits={[...this.fruits]} />
        </div>
        <div id="red fruit names">
          <FrutaCor fruits={[...this.fruits]} />
        </div>
        <div id="total">
          <FrutaRed fruits={[...this.fruits]} />
        </div>
      </div>
    );
  }
}

export default App;
