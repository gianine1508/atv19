import React from "react";

class FrutaCor extends React.Component {
  render() {
    const fruits = this.props.fruits;
    return (
      <ul>
        {fruits
          .filter(({ color }) => color === "red")
          .map(({ name }, index) => (
            <li key={index}>{name}</li>
          ))}
      </ul>
    );
  }
}

export default FrutaCor;
