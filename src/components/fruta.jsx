import React from "react";

class Fruta extends React.Component {
  render() {
    const fruits = this.props.fruits;
    return (
      <ul>
        {fruits.map(({ name }, index) => (
          <li key={index}>{name}</li>
        ))}
      </ul>
    );
  }
}

export default Fruta;
