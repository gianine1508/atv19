import { Component } from "react";

class FrutaRed extends Component {
  render() {
    const fruits = this.props.fruits;
    return (
      <ul>
        <li>
          {fruits.reduce((acumulador, { price }) => {
            return acumulador + price;
          }, 0)}
        </li>
      </ul>
    );
  }
}

export default FrutaRed;
